import { GetProductsForIngredient, GetRecipes } from "./supporting-files/data-access";
import { NutrientFact, Product, RecipeLineItem, SupplierProduct } from "./supporting-files/models";
import { GetCostPerBaseUnit, SumUnitsOfMeasure } from "./supporting-files/helpers";
import { RunTest, ExpectedRecipeSummary } from "./supporting-files/testing";
import { UnitOfMeasure,} from "./supporting-files/models";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
let recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

let productNutrients : NutrientFact[] = [];
let recepiePrice = 0;

// loop on line items of recipes 
recipeData.forEach((recipe) => {
    recipe.lineItems.forEach(( item : RecipeLineItem)=>{
        getCheapestProduct(item); 
    });
});

let prodNutrients = {};
productNutrients.forEach(( p ) =>{
    prodNutrients[p.nutrientName] = p
});
const recipeName = recipeData[0].recipeName;
recipeSummary[`${recipeName}`] = {'cheapestCost': recepiePrice, 'nutrientsAtCheapestCost': prodNutrients}
recipeSummary[`${recipeName}`]['nutrientsAtCheapestCost'] = getSortedNutrients(); 


function getCheapestProduct(obj: any) {
    //cheapest product found
    const products : Product[] = GetProductsForIngredient(obj.ingredient);
    let cheapestProductPrice: number = 0;
    let cheapProductNutrients: any;
    products.forEach(( p : Product) =>{
        // get supplier cheapest product for lineitem
        const supplierLowest : SupplierProduct = p.supplierProducts.sort(
            (a : SupplierProduct,b : SupplierProduct) =>  GetCostPerBaseUnit(a)  - GetCostPerBaseUnit(b))[0];
        const supplier_account  =  GetCostPerBaseUnit(supplierLowest);
        if( (supplier_account * obj.unitOfMeasure.uomAmount) < cheapestProductPrice ||  cheapestProductPrice === 0){
            // get item more cheaper, subtract previous supplier price of current recepie item and add new item price
            recepiePrice = recepiePrice - cheapestProductPrice;
            cheapestProductPrice  =  supplier_account * obj.unitOfMeasure.uomAmount;
            cheapProductNutrients = p.nutrientFacts;
            recepiePrice = recepiePrice + cheapestProductPrice;
        }
    });
    retrieveProductNutrients(cheapProductNutrients)
    return cheapProductNutrients;
}

// sort nutrient value of recepie 
function getSortedNutrients(){
    const sortedNutrient = {};
    const sortedKeys = Object.keys(recipeSummary[`${recipeName}`]['nutrientsAtCheapestCost']).sort();
    for (const key of sortedKeys) {
        sortedNutrient[key] = recipeSummary[`${recipeName}`]['nutrientsAtCheapestCost'][key];
    }
    return sortedNutrient
}

function getUnitMeasurement(unit1: UnitOfMeasure, unit2: UnitOfMeasure){
    return SumUnitsOfMeasure(unit1, unit2);
}

function retrieveProductNutrients(cheapProductNutrients: any){
    //nutrientFacts for the product
    cheapProductNutrients.forEach(( prod:any ) =>{
        const nutrient = productNutrients.filter((nutr) => nutr.nutrientName.includes(prod.nutrientName));
        // nutrient already added in a list then update the value
        if (nutrient.length > 0){
            try{nutrient[0].quantityAmount.uomAmount += getUnitMeasurement(prod.quantityPer, prod.quantityAmount)['uomAmount']-prod.quantityPer['uomAmount'];} 
            catch{nutrient[0].quantityAmount.uomAmount += getUnitMeasurement(prod.quantityAmount, prod.quantityPer)['uomAmount']-prod.quantityPer['uomAmount'];}
            nutrient[0].quantityAmount.uomAmount = nutrient[0].quantityAmount.uomAmount < 1 ? Number(nutrient[0].quantityAmount.uomAmount.toPrecision(1)) : Number(nutrient[0].quantityAmount.uomAmount.toFixed(1));
        }
        else {
            try{ prod.quantityAmount = getUnitMeasurement(prod.quantityPer, prod.quantityAmount);} 
            catch{prod.quantityAmount = getUnitMeasurement(prod.quantityAmount, prod.quantityPer);}
            prod.quantityAmount.uomAmount -= prod.quantityPer['uomAmount'];
            productNutrients = productNutrients.concat(prod);
            prod.quantityAmount.uomAmount =  prod.quantityAmount.uomAmount < 1 ? Number(prod.quantityAmount.uomAmount.toPrecision(1)) : Number(prod.quantityAmount.uomAmount.toFixed(1));
        }
    });
}

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
